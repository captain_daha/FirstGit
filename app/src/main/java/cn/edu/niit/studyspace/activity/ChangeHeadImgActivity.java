package cn.edu.niit.studyspace.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.edu.niit.studyspace.R;

/**
 * @author cky
 * date 2019-11-01
 */
public class ChangeHeadImgActivity extends Activity implements AdapterView.OnItemClickListener {
    private GridView gvHead;
    private List<Map<String, Object>> headList;
    private SimpleAdapter simpleAdapter;

    int[] headId = {R.mipmap.head1, R.mipmap.head2, R.mipmap.head3, R.mipmap.head4, R.mipmap.head5, R.mipmap.head6, R.mipmap.head7, R.mipmap.head8, R.mipmap.head9};

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_head_img);
        initData();
        initView();

    }

    private void initView() {
        gvHead = findViewById(R.id.gv_head);
        String[] from = {"img", "text"};
        int[] to = {R.id.iv_head_img, R.id.tv_text};
        simpleAdapter = new SimpleAdapter(this, headList, R.layout.item_head, from, to);
        gvHead.setAdapter(simpleAdapter);
        gvHead.setOnItemClickListener(this);
    }

    /**
     * 填充图片资源
     */
    private void initData() {
        headList = new ArrayList<>();
        for (int i = 0; i < headId.length; i++) {
            Map<String, Object> map = new HashMap<>();
            map.put("img", headId[i]);
            headList.add(map);
        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Intent intent = getIntent();
        intent.putExtra("img", headId[i]);
        setResult(InfoActivity.REQUESTION, intent);
        Toast.makeText(this, "更换头像成功", Toast.LENGTH_SHORT).show();
        finish();
    }
}
