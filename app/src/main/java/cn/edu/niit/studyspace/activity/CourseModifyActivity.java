package cn.edu.niit.studyspace.activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import org.litepal.crud.DataSupport;

import cn.edu.niit.studyspace.R;
import cn.edu.niit.studyspace.bean.Course;

/**
 * @Author: 杨为智
 * @Description:
 * @Date: Create in 14:07 2019/11/6
 * Spica 27° is very HappyToday!
 */
public class CourseModifyActivity extends AppCompatActivity {
    private EditText etName;
    private EditText etPlace;
    private EditText etInfo;
    private EditText etTeacher;
    private Button btnSave;
    private long id;
    private Course course;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_course);
        initView();
        getData();
        initListener();
    }

    private void initView() {
        etInfo = findViewById(R.id.et_course_info);
        etName = findViewById(R.id.et_course_name);
        etPlace = findViewById(R.id.et_course_place);
        etTeacher = findViewById(R.id.et_course_teacher);
        btnSave = findViewById(R.id.btn_save_course);
    }

    private void getData() {
        Intent intent = getIntent();
        id = intent.getLongExtra("cId", -1);
        if (id == -1) {
            Toast.makeText(this, "获取信息失败", Toast.LENGTH_SHORT).show();
        } else {
            course = DataSupport.find(Course.class, id, true);
            etInfo.setText(course.getInfo());
            etName.setText(course.getName());
            etPlace.setText(course.getPlace());
            etTeacher.setText(course.getTeacher());
        }


    }

    private void initListener() {
        btnSave.setOnClickListener(view -> {
            //TODO---点击保存------
            Course course = new Course();
            course.setInfo(etInfo.getText().toString().trim());
            course.setName(etName.getText().toString().trim());
            course.setPlace(etPlace.getText().toString().trim());
            course.setTeacher(etTeacher.getText().toString().trim());
            course.update(id);
            Toast.makeText(this, "保存成功", Toast.LENGTH_SHORT).show();

        });
    }


}
