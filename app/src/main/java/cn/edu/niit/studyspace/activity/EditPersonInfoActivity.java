package cn.edu.niit.studyspace.activity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import cn.edu.niit.studyspace.R;
import cn.edu.niit.studyspace.application.MyApplication;
import cn.edu.niit.studyspace.bean.User;

/**
 * @author cky
 * date 2019-11-01
 */
public class EditPersonInfoActivity extends AppCompatActivity {
    ImageView ivBack;
    EditText etUsername;
    RadioGroup radioGroup;
    RadioButton rbMan;
    RadioButton rbWomen;
    RadioButton rbUnknown;
    EditText etPhoneNum;
    CheckBox cBoxJava;
    CheckBox cBoxC;
    CheckBox cBoxAndroid;
    CheckBox cBoxPHP;
    Button btnSave;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_person_info);
        initView();
    }

    private void initView() {
        ivBack = findViewById(R.id.iv_back);
        ivBack.setOnClickListener(v -> finish());

        etUsername = findViewById(R.id.et_username);
        radioGroup = findViewById(R.id.rdo_group);
        rbMan = findViewById(R.id.rb_man);
        rbWomen = findViewById(R.id.rb_women);
        rbUnknown = findViewById(R.id.rb_unknown);
        etPhoneNum = findViewById(R.id.et_phonenum);
        cBoxJava = findViewById(R.id.cBox_Java);
        cBoxC = findViewById(R.id.cBox_C);
        cBoxAndroid = findViewById(R.id.cBox_Android);
        cBoxPHP = findViewById(R.id.cBox_PHP);
        btnSave = findViewById(R.id.btn_save);

        //注入用户个人信息
        injectMsg();
        //保存按钮的点击事件
        btnSave.setOnClickListener(v -> saveMsg());

    }

    private void saveMsg() {
        MyApplication myApplication = (MyApplication) getApplication();
        User user = myApplication.getCurrentUser();
        String nickname = etUsername.getText().toString();//获取昵称
        if(nickname.isEmpty()){
            Toast.makeText(myApplication, "昵称不能为空", Toast.LENGTH_SHORT).show();
            return;
        }
        user.setNickname(nickname);

        switch (radioGroup.getCheckedRadioButtonId()) {
            case R.id.rb_man:
                user.setGender(1);
                break;
            case R.id.rb_women:
                user.setGender(2);
                break;
            case R.id.rb_unknown:
                user.setGender(0);
                break;
        }

        user.setPhoneNum(etPhoneNum.getText().toString());

        StringBuilder sb = new StringBuilder();
        if (cBoxJava.isChecked()) {
            sb.append("Java,");
        }
        if (cBoxC.isChecked()) {
            sb.append("C++,");
        }
        if (cBoxAndroid.isChecked()) {
            sb.append("Android,");
        }
        if (cBoxPHP.isChecked()) {
            sb.append("PHP");
        }
        String lessonStr = sb.toString();
        if(sb != null){
            if (lessonStr.endsWith(",")) {
                String sub = lessonStr.substring(0, lessonStr.length() - 1);
                user.setFavorLessons(sub);
            }else {
                user.setFavorLessons(lessonStr);
            }
        }

        //修改数据库中的用户信息
        user.update(user.getId());

        myApplication.setCurrentUser(user);
        Toast.makeText(myApplication, "保存成功", Toast.LENGTH_SHORT).show();
        finish();
    }

    /**
     *
     */
    private void injectMsg() {
        MyApplication myApplication = (MyApplication) getApplication();
        User user = myApplication.getCurrentUser();
        if (user != null) {
            etUsername.setText(user.getUsername());
            etUsername.setSelection(user.getUsername().length());
            switch (user.getGender()) {
                case 0:
                    rbUnknown.setChecked(true);
                    break;
                case 1:
                    rbMan.setChecked(true);
                    break;
                case 2:
                    rbWomen.setChecked(true);
                    break;
            }
            etPhoneNum.setText(user.getPhoneNum());
            String favorLessons = user.getFavorLessons();
            if (favorLessons == null) {
                return;
            }
            String[] lessonArray = favorLessons.split(",");
            if (lessonArray.length == 0) {
                return;
            } else {
                for (String lesson : lessonArray) {
                    switch (lesson) {
                        case "Java":
                            cBoxJava.setChecked(true);
                            break;
                        case "C++":
                            cBoxC.setChecked(true);
                            break;
                        case "Android":
                            cBoxAndroid.setChecked(true);
                            break;
                        case "PHP":
                            cBoxPHP.setChecked(true);
                            break;
                    }
                }
            }
        }
    }
}
