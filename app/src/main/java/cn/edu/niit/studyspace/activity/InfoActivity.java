package cn.edu.niit.studyspace.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import cn.edu.niit.studyspace.R;
import cn.edu.niit.studyspace.application.MyApplication;
import cn.edu.niit.studyspace.bean.User;
import cn.edu.niit.studyspace.view.CircleImageView;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

public class InfoActivity extends AppCompatActivity {
    CircleImageView ivProfile;
    ImageView ivEdit;
    TextView tvUsername;
    TextView tvSex;
    TextView tvPhone;
    TextView tvFavorLessons;

    final static int REQUESTION = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);
        initView();
    }

    private void initView() {
        ivProfile = findViewById(R.id.iv_profile);
        ivEdit = findViewById(R.id.iv_edit);
        tvUsername = findViewById(R.id.tv_username);
        tvSex = findViewById(R.id.tv_sex);
        tvPhone = findViewById(R.id.tv_phone);
        tvFavorLessons = findViewById(R.id.tv_favor_lesson);

        ivProfile.setOnClickListener(v -> {
            Intent intent = new Intent(InfoActivity.this, ChangeHeadImgActivity.class);
            startActivityForResult(intent, REQUESTION);
        });

        ivEdit.setOnClickListener(v -> {
            startActivity(new Intent(InfoActivity.this, EditPersonInfoActivity.class));
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        setUserMsg();
    }

    private void setUserMsg() {
        MyApplication myApplication = (MyApplication) getApplication();
        User user = myApplication.getCurrentUser();
        tvUsername.setText(user.getNickname());
        switch (user.getGender()) {
            case 0:
                tvSex.setText("性别：未知");
                break;
            case 1:
                tvSex.setText("性别：男生");
                break;
            case 2:
                tvSex.setText("性别：女生");
                break;
        }
        if (user.getPhoneNum() == null) {
            tvPhone.setText("电话：************");
        } else {
            tvPhone.setText("电话：" + user.getPhoneNum());
        }
        if (user.getFavorLessons() == null) {
            tvFavorLessons.setText("我最喜爱的课程：无");
        } else {
            tvFavorLessons.setText("我最喜爱的课程：" + user.getFavorLessons());
        }
    }

    public void logout(View view) {
       Intent intent = new Intent(this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUESTION) {
            if (data != null) {
                int img = data.getIntExtra("img", 0);
                ivProfile.setImageResource(img);
            }
        }
    }
}
