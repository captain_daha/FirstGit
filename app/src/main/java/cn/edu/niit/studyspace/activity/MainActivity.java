package cn.edu.niit.studyspace.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import cn.edu.niit.studyspace.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void intoToolBox(View view) {
        startActivity(new Intent(this, ToolBoxActivity.class));
    }

    public void intoCourse(View view) {
        startActivity(new Intent(this, CourseActivity.class));
    }

    public void intoMusic(View view) {
        startActivity(new Intent(this, MusicActivity.class));
    }

    public void intoNoted(View view) {
        startActivity(new Intent(this, NoteActivity.class));
    }

    public void intoInfo(View view) {
        startActivity(new Intent(this, InfoActivity.class));
    }
}
