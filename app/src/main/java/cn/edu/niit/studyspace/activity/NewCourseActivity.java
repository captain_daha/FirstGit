package cn.edu.niit.studyspace.activity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import cn.edu.niit.studyspace.R;
import cn.edu.niit.studyspace.bean.Course;

public class NewCourseActivity extends AppCompatActivity {
  private EditText etName;
  private EditText etPlace;
  private EditText etInfo;
  private EditText etTeacher;
  private Button btnSave;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_new_course);
    initView();
    initView();
    initListener();
  }

  private void initView() {
    etInfo = findViewById(R.id.et_course_info);
    etName = findViewById(R.id.et_course_name);
    etPlace = findViewById(R.id.et_course_place);
    etTeacher = findViewById(R.id.et_course_teacher);
    btnSave = findViewById(R.id.btn_save_course);
  }

  private void initListener() {
    btnSave.setOnClickListener(
        view -> {
          // TODO---点击保存------
          Course course = new Course();
          course.setInfo(etInfo.getText().toString().trim());
          course.setName(etName.getText().toString().trim());
          course.setPlace(etPlace.getText().toString().trim());
          course.setTeacher(etTeacher.getText().toString().trim());
          if (course.save()) {
            Toast.makeText(NewCourseActivity.this, "保存成功", Toast.LENGTH_SHORT).show();
            finish();
          } else {
            Toast.makeText(NewCourseActivity.this, "保存失败", Toast.LENGTH_SHORT).show();
          }
        });
  }
}
