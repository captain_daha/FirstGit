package cn.edu.niit.studyspace.activity;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.sql.Date;
import java.text.SimpleDateFormat;

import cn.edu.niit.studyspace.R;
import cn.edu.niit.studyspace.bean.Note;

public class NewNoteActivity extends AppCompatActivity {

    TextView title;
    TextView content;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_note);
        title = findViewById(R.id.tv_title);
        content = findViewById(R.id.tv_content);
    }

    @Override
    public void onBackPressed() {
        String titleString = title.getText().toString();
        String contentString = content.getText().toString();
        if (("").equals(titleString) && ("").equals(contentString)) {
            super.onBackPressed();
        } else {
            Note note = new Note();
            note.setTitle(titleString);
            note.setContent(contentString);
            Date d = new Date(System.currentTimeMillis());
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            note.setDate(sdf.format(d));
            note.save();
            finish();
        }
    }
}
