package cn.edu.niit.studyspace.activity;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.animation.ValueAnimator;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.OvershootInterpolator;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import cn.edu.niit.studyspace.R;
import cn.edu.niit.studyspace.bean.SchoolTableBean;
import cn.edu.niit.studyspace.bean.SchoolTableItemBean;

public class NewSchoolTableActivity extends AppCompatActivity {
  private EditText etTableName;
  private EditText etTableInfo;
  private Button btnSave;
  private View inputLayout;
  private float width, height;
  View progress;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_new_school_table);
    initView();
    initListener();
  }

  private void initView() {
    etTableInfo = findViewById(R.id.et_table_info);
    etTableName = findViewById(R.id.et_table_name);
    btnSave = findViewById(R.id.btn_sure);
    progress = findViewById(R.id.layout_progress);
    inputLayout = findViewById(R.id.input_layout);
  }

  private void initListener() {
    btnSave.setOnClickListener(
        view -> {
          SchoolTableBean tableBean = new SchoolTableBean();
          if (etTableName.getText().toString().trim().equals("")
              || etTableInfo.getText().toString().trim().equals("")) {
            mToast("请填充数据");
          }
          tableBean.setName(etTableName.getText().toString().trim());
          tableBean.setSubName(etTableInfo.getText().toString().trim());

          for (int i = 0; i < 40; i++) {
            SchoolTableItemBean itemBean = new SchoolTableItemBean();
            itemBean.setName("未填写" + i);
            itemBean.setInfo("未填写" + i);
            itemBean.setPlace("未填写" + i);
            itemBean.setTeacher("未填写" + i);


            if (itemBean.save()) {
              Log.d("保存一次", "");
              tableBean.getItems().add(itemBean);
                width = inputLayout.getMeasuredWidth();
                height = inputLayout.getMeasuredHeight();
                etTableInfo.setVisibility(View.INVISIBLE);
                etTableName.setVisibility(View.INVISIBLE);
                inputAnimator(inputLayout, width, height);

            } else {
              Log.e("保存失败", "");
            }
          }
          tableBean.save();
        });
  }

  private void inputAnimator(final View view, float w, float h) {

    AnimatorSet set = new AnimatorSet();

    ValueAnimator animator = ValueAnimator.ofFloat(0, w);
    animator.addUpdateListener(
        new ValueAnimator.AnimatorUpdateListener() {

          @Override
          public void onAnimationUpdate(ValueAnimator animation) {
            float value = (Float) animation.getAnimatedValue();
            ViewGroup.MarginLayoutParams params =
                (ViewGroup.MarginLayoutParams) view.getLayoutParams();
            params.leftMargin = (int) value;
            params.rightMargin = (int) value;
            view.setLayoutParams(params);
          }
        });

    ObjectAnimator animator3 = ObjectAnimator.ofFloat(inputLayout, "scaleX", 1f, 0.5f);
      ObjectAnimator animator2 = ObjectAnimator.ofFloat(inputLayout, "scaleY", 1f, 0.5f);
    set.setDuration(1000);
    set.setInterpolator(new AccelerateDecelerateInterpolator());
    set.playTogether(animator, animator2,animator3);
    set.start();
    set.addListener(
        new Animator.AnimatorListener() {

          @Override
          public void onAnimationStart(Animator animation) {}

          @Override
          public void onAnimationRepeat(Animator animation) {}

          @Override
          public void onAnimationEnd(Animator animation) {
            progress.setVisibility(View.VISIBLE);
            progressAnimator(progress);
            inputLayout.setVisibility(View.INVISIBLE);
          }

          @Override
          public void onAnimationCancel(Animator animation) {}
        });
  }

  private void progressAnimator(final View view) {
    PropertyValuesHolder animator = PropertyValuesHolder.ofFloat("scaleX", 0.8f, 1f);
    PropertyValuesHolder animator2 = PropertyValuesHolder.ofFloat("scaleY", 0.8f, 1f);
    ObjectAnimator animator3 = ObjectAnimator.ofPropertyValuesHolder(view, animator, animator2);
    animator3.setDuration(1000);
    animator3.setInterpolator(new OvershootInterpolator());
    animator3.start();

    animator3.addListener(new Animator.AnimatorListener() {
        @Override
        public void onAnimationStart(Animator animation) {

        }

        @Override
        public void onAnimationEnd(Animator animation) {
            finish();
            Toast.makeText(NewSchoolTableActivity.this,"创建成功",Toast.LENGTH_SHORT).show();

        }

        @Override
        public void onAnimationCancel(Animator animation) {

        }

        @Override
        public void onAnimationRepeat(Animator animation) {

        }
    });

  }

  private void mToast(@NonNull String values) {
    Toast.makeText(this, values, Toast.LENGTH_SHORT).show();
  }
}
