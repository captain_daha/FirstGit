package cn.edu.niit.studyspace.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.RecyclerView;

import org.litepal.crud.DataSupport;

import java.util.ArrayList;
import java.util.List;

import cn.edu.niit.studyspace.R;
import cn.edu.niit.studyspace.adapter.NoteAdapter;
import cn.edu.niit.studyspace.bean.Note;

public class NoteActivity extends AppCompatActivity {

  RecyclerView noteListView;
  NoteAdapter noteAdapter;

  List<Note> notes = new ArrayList<>();

  @Override
  protected void onResume() {
    super.onResume();
    getAllNotes();
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_note);
    initView();
    initData();
  }

  private void initData() {
    getAllNotes();
  }

  private void getAllNotes() {
    notes = DataSupport.findAll(Note.class);
    noteAdapter.notifyData(notes);
  }

  private void initView() {
    noteListView = findViewById(R.id.rv_note);
    noteAdapter = new NoteAdapter(this, notes);
    noteListView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
    noteListView.setAdapter(noteAdapter);
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    noteAdapter = null;
  }

  public void addNote(View view) {
    startActivity(new Intent(this, NewNoteActivity.class));
  }
}
