package cn.edu.niit.studyspace.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.litepal.crud.DataSupport;

import cn.edu.niit.studyspace.R;
import cn.edu.niit.studyspace.adapter.SchoolTableDetailAdapter;
import cn.edu.niit.studyspace.bean.SchoolTableBean;

public class SchoolTimeTableDetailActivity extends AppCompatActivity {
    private int id;
    SchoolTableBean tableBean;
    SchoolTableDetailAdapter adapter;

    RecyclerView rcvSchoolTableDetail;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_school_time_table_detail);
        rcvSchoolTableDetail=findViewById(R.id.rcv_shaool_table);
        initData();
        initView();
    }

    private void initData() {
        Intent intent = getIntent();
        id = intent.getIntExtra("tid", 0);
        Log.d("获得id: ", id + "");
        tableBean = DataSupport.find(SchoolTableBean.class, id, true);

    }



    private void initView() {
        GridLayoutManager layoutManager=new GridLayoutManager(this,5);
        rcvSchoolTableDetail.setLayoutManager(layoutManager);
        adapter=new SchoolTableDetailAdapter(this,tableBean.getItems());
        rcvSchoolTableDetail.setAdapter(adapter);


    }

    @Override
    protected void onResume() {
        super.onResume();
        tableBean = DataSupport.find(SchoolTableBean.class, id, true);
        adapter.notifyData(tableBean.getItems());
        initData();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (adapter != null) {
            adapter = null;
        }
        System.runFinalization();
    }
}
