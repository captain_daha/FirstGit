package cn.edu.niit.studyspace.activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import org.w3c.dom.Text;

import java.util.Timer;
import java.util.TimerTask;

import cn.edu.niit.studyspace.R;

public class SplashActivity extends AppCompatActivity {
    TextView tv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        tv = findViewById(R.id.tv);
        Typeface typeFace =Typeface.createFromAsset(getAssets(),"new.ttf");
        tv.setTypeface(typeFace);

        TimerTask timerTask = new TimerTask() {

            @Override
            public void run() {
                Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
            }

        };

        new Timer().schedule(timerTask, 3000);
    }
}
