package cn.edu.niit.studyspace.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.PopupMenu;
import androidx.recyclerview.widget.RecyclerView;

import org.litepal.crud.DataSupport;

import java.lang.ref.WeakReference;
import java.util.List;

import cn.edu.niit.studyspace.R;
import cn.edu.niit.studyspace.activity.SchoolTimeTableDetailActivity;
import cn.edu.niit.studyspace.bean.SchoolTableBean;

public class SchoolTableAdapter extends RecyclerView.Adapter<SchoolTableAdapter.ViewHolder> {

  private WeakReference<Context> wr;
  private List<SchoolTableBean> items;

  public SchoolTableAdapter(Context context, List<SchoolTableBean> schoolTableBeans) {
    wr = new WeakReference<>(context);
    this.items = schoolTableBeans;
  }

  @NonNull
  @Override
  public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(wr.get()).inflate(R.layout.item_school_table, parent, false);
    return new ViewHolder(view);
  }

  @Override
  public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
    SchoolTableBean schoolTableBean = items.get(position);

    viewHolder.title.setText(schoolTableBean.getName());
    viewHolder.subTitle.setText(schoolTableBean.getSubName());
    viewHolder.btn_item.setOnClickListener(
        view -> {
          // todo------进入详情-------
          Intent intent = new Intent(wr.get(), SchoolTimeTableDetailActivity.class);
          intent.putExtra("tid", schoolTableBean.getId());
          wr.get().startActivity(intent);
        });
    viewHolder.button.setOnClickListener(
        view -> showPopupMenu(viewHolder.button, schoolTableBean.getId()));
  }

  @Override
  public int getItemCount() {
    return items.size();
  }

  public void notifyData(List<SchoolTableBean> schoolTableBeans) {
    this.items = schoolTableBeans;
    notifyDataSetChanged();
  }

  static class ViewHolder extends RecyclerView.ViewHolder {
    Button button;
    TextView title;
    TextView subTitle;
    Button btn_item;

    public ViewHolder(@NonNull View itemView) {
      super(itemView);
      subTitle = itemView.findViewById(R.id.tv_subTitle);
      title = itemView.findViewById(R.id.tv_title);
      button = itemView.findViewById(R.id.btn_more);
      btn_item = itemView.findViewById(R.id.btn_item);
    }
  }

  private void showPopupMenu(View view, int id) {
    PopupMenu popupMenu = new PopupMenu(wr.get(), view);
    popupMenu.inflate(R.menu.menu_item);
    popupMenu.setOnMenuItemClickListener(
        menuItem -> {
          switch (menuItem.getItemId()) {
            case R.id.action_delete:
              // 删除
              DataSupport.delete(SchoolTableBean.class, id);
              Toast.makeText(wr.get(), "删除", Toast.LENGTH_SHORT).show();
              items.clear(); // 清空
              items.addAll(DataSupport.findAll(SchoolTableBean.class, true)); // 更新
              this.notifyData(items);
              return true;
            case R.id.action_modify:
              // 修改
              Toast.makeText(wr.get(), "修改", Toast.LENGTH_SHORT).show();
              return true;
            default:
          }
          return false;
        });
    popupMenu.show();
  }
}
