package cn.edu.niit.studyspace.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.lang.ref.WeakReference;
import java.util.List;

import cn.edu.niit.studyspace.R;
import cn.edu.niit.studyspace.activity.SchoolTimeTableItemModifyActivity;
import cn.edu.niit.studyspace.bean.SchoolTableItemBean;

public class SchoolTableDetailAdapter extends RecyclerView.Adapter<SchoolTableDetailAdapter.ViewHolder> {
    private WeakReference<Context> wr;
    private List<SchoolTableItemBean> items;

    public SchoolTableDetailAdapter(Context context, List<SchoolTableItemBean> items){
        wr=new WeakReference<>(context);
        this.items=items;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(wr.get()).inflate(R.layout.item_shool_table_detail,parent,false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        SchoolTableItemBean itemBean=items.get(position);
    holder.view.setOnClickListener(
        new View.OnClickListener() {
          @Override
          public void onClick(View v) {
            Intent intent = new Intent(wr.get(), SchoolTimeTableItemModifyActivity.class);
            intent.putExtra("dId", itemBean.getId());
            Log.e( "onClick: a", itemBean.getId()+"");
          wr.get().startActivity(intent);
          }
        });

        holder.tvSubTitle.setText(itemBean.getTeacher());
        holder.tvTitle.setText(itemBean.getName());
        holder.tvInfo.setText(itemBean.getInfo());


    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void notifyData(List<SchoolTableItemBean> items) {
        this.items = items;
        this.notifyDataSetChanged();
    }



    static  class ViewHolder extends RecyclerView.ViewHolder{
        private TextView tvTitle;
        private TextView tvSubTitle;
        private TextView tvInfo;
        private View view;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvInfo=itemView.findViewById(R.id.tv_info);
            tvTitle=itemView.findViewById(R.id.tv_title);
            tvSubTitle=itemView.findViewById(R.id.tv_subtitle);
            view=itemView;

        }
    }
}
