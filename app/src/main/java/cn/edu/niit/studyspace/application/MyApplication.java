package cn.edu.niit.studyspace.application;

import android.app.Application;

import org.litepal.LitePal;

import cn.edu.niit.studyspace.bean.User;

public class MyApplication extends Application {
    private User currentUser;

    @Override
    public void onCreate() {
        super.onCreate();
        LitePal.initialize(this);
    }

    public User getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }
}
