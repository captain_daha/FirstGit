package cn.edu.niit.studyspace.bean;

import java.util.ArrayList;

/**
 * @Author: 杨为智
 * @Description:
 * @Date: Create in 9:27 2019/11/5
 * Spica 27° is very HappyToday!
 */
public class ListViewBean {
  private ArrayList<SchoolTableItemBean> schoolTableItemBeans = new ArrayList<>();

  public ArrayList<SchoolTableItemBean> getSchoolTableItemBeans() {
    return schoolTableItemBeans;
  }

  public void setSchoolTableItemBeans(ArrayList<SchoolTableItemBean> schoolTableItemBeans) {
    this.schoolTableItemBeans = schoolTableItemBeans;
  }
}
