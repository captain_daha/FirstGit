package cn.edu.niit.studyspace.bean;



import org.litepal.annotation.Column;
import org.litepal.crud.DataSupport;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author: 杨为智
 * @Description:
 * @Date: Create in 7:51 2019/11/5
 * Spica 27° is very HappyToday!
 */
public class SchoolTableBean extends DataSupport {
  private int id;
  @Column(unique = true, defaultValue = "未填写")
  private String name;
  private String subName;
  private List<SchoolTableItemBean> items=new ArrayList<SchoolTableItemBean>();

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getSubName() {
    return subName;
  }

  public void setSubName(String subName) {
    this.subName = subName;
  }

  public List<SchoolTableItemBean> getItems() {
    return items;
  }

  public void setItems(List<SchoolTableItemBean> items) {
    this.items = items;
  }
}
