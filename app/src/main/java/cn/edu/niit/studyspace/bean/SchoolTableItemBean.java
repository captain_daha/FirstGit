package cn.edu.niit.studyspace.bean;

import org.litepal.crud.DataSupport;

/**
 * @Author: 杨为智
 * @Description:
 * @Date: Create in 7:53 2019/11/5
 * Spica 27° is very HappyToday!
 */
public class SchoolTableItemBean extends DataSupport {
  private long id;

  private String name;//课程名称

  private String place;//地点

  private String teacher;//老师名称

  private String info;//备注信息

  public void setName(String name) {
    this.name = name;
  }

  public void setPlace(String place) {
    this.place = place;
  }

  public void setTeacher(String teacher) {
    this.teacher = teacher;
  }

  public void setInfo(String info) {
    this.info = info;
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }


  public String getName() {
    return name;
  }
  public String getPlace() {
    return place;
  }

  public String getTeacher() {
    return teacher;
  }

  public String getInfo() {
    return info;
  }

}
