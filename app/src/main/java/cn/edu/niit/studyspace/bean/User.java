package cn.edu.niit.studyspace.bean;

import org.litepal.crud.DataSupport;

import java.io.Serializable;

public class User extends DataSupport implements Serializable {
    private int id;
    private String username;
    private String nickname;//昵称
    private String password;
    private String date;//注册时间
    private String phoneNum;
    private int gender; //0-unknown；1-男；2-女
    private String favorLessons;//最喜欢的课程，用逗号分隔，例子：“java,c,c++,html”

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getPhoneNum() {
        return phoneNum;
    }

    public void setPhoneNum(String phoneNum) {
        this.phoneNum = phoneNum;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public String getFavorLessons() {
        return favorLessons;
    }

    public void setFavorLessons(String favorLessons) {
        this.favorLessons = favorLessons;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", nickname='" + nickname + '\'' +
                ", password='" + password + '\'' +
                ", date='" + date + '\'' +
                ", phoneNum='" + phoneNum + '\'' +
                ", gender=" + gender +
                ", favorLessons='" + favorLessons + '\'' +
                '}';
    }
}
