package cn.edu.niit.studyspace.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.transition.MaterialContainerTransformSharedElementCallback;

import org.litepal.crud.DataSupport;

import java.util.ArrayList;
import java.util.List;

import cn.edu.niit.studyspace.R;
import cn.edu.niit.studyspace.activity.NewSchoolTableActivity;
import cn.edu.niit.studyspace.adapter.SchoolTableAdapter;
import cn.edu.niit.studyspace.bean.SchoolTableBean;

/** @Author: 杨为智 @Description: @Date: Create in 17:23 2019/10/31 Spica 27° is very HappyToday! */
public class SchoolTimetableFragment extends Fragment {
  private Context context;
  private RecyclerView rvSchoolTable;
  private FloatingActionButton fabAddTable;
  SchoolTableAdapter adapter;
  ArrayList<SchoolTableBean> items = new ArrayList<>();

  @Nullable
  @Override
  public View onCreateView(
      @NonNull LayoutInflater inflater,
      @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_shool_timetable, container, false);
    context = getActivity(); // 获取上下文
    rvSchoolTable = view.findViewById(R.id.rv_school_table);
    fabAddTable = view.findViewById(R.id.fab_add_table);
    ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("课程表");
    initData();
    initView();
    return view;
  }

  private void initView() {
    adapter = new SchoolTableAdapter(context, items);
    rvSchoolTable.addItemDecoration(
        new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));

    rvSchoolTable.setAdapter(adapter);

    fabAddTable.setOnClickListener(
        view -> {



          startActivity(new Intent(context, NewSchoolTableActivity.class));

        });
  }

  private void initData() {
    List<SchoolTableBean> allBeans = DataSupport.findAll(SchoolTableBean.class);
    items.clear();
    for (SchoolTableBean tableBean : allBeans) {
      Log.d(getClass().getName(), "--添加单元--" + tableBean.getName());
      items.add(tableBean);
    }
  }

  @Override
  public void onResume() {
    super.onResume();
    initData();
    adapter.notifyData(items);
  }
}
