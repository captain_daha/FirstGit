package cn.edu.niit.studyspace.util;

public class DateUtil {
    /**
     * 将long值转换为以小时计算的格式
     *
     * @param mss
     * @return
     */
    public static String formatLongTime(long mss) {
        String DateTimes = null;
        long minutes = (mss % (60 * 60)) / 60;
        long seconds = mss % 60;

        DateTimes = String.format("%02d:",
                minutes) + String.format("%02d", seconds);
        return DateTimes;
    }
}
